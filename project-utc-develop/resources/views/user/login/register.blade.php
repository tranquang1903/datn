<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> Free Dental Medical Hospital Website Template | Smarteyeapps.com</title>

    <link rel="shortcut icon" href="/template/loginuser/images/fav.jpg">
    <link rel="stylesheet" href="/template/loginuser/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/loginuser/css/fontawsom-all.min.css">
    <link rel="stylesheet" type="text/css" href="/template/loginuser/css/style.css" />
</head>

<body class="h-100">
<div class="container-fluid full-bg h-100">
    <div class="container h-100">
        <div class="row no-margin h-100">
            <div class="bg-layer d-flex col-md-4">
                <div class="login-box row">
                    <form id="addCateForm" action="/user/register/create" method="post"
                          accept-charset="UTF-8"
                    >
                    <h3>User Registeration</h3>
                        @error('name')
                        <div class="form-check text-danger">{{ $message }}</div>
                        @enderror
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control" placeholder="Full Name" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                        @error('email')
                        <div class="form-check text-danger">{{ $message }}</div>
                        @enderror
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
                        </div>
                        <input type="text" name="email" value="{{old('email')}}" class="form-control" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                        @error('password')
                        <div class="form-check text-danger">{{ $message }}</div>
                        @enderror
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                        </div>
                        <input  name="password"
                               type="password" class="form-control" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                        @error('password')
                        <div class="form-check text-danger">{{ $message }}</div>
                        @enderror
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Repeat Password" aria-label="Username" aria-describedby="basic-addon1">
                    </div>




                    <button type="submit" class="btn btn-success">Click to Login</button>

                    <p class="no-c">Already have Account? <a href="/user/login">Sign In</a></p>
                        @csrf
                    </form>
                </div>
            </div>
        </div>

        <div class="foter-credit">
            <a href="https://smarteyeapps.com/">Designed by : Smarteyeapps.com</a>
        </div>
    </div>

</div>
</body>

<script src="/template/loginuser/js/jquery-3.2.1.min.js"></script>
<script src="/template/loginuser/js/popper.min.js"></script>
<script src="/template/loginuser/js/bootstrap.min.js"></script>
<script src="/template/loginuser/js/script.js"></script>


</html>
