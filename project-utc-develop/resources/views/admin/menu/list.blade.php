@extends('admin.main')

@section('content')
    @php
        $stt = (($_GET['page'] ?? 1) - 1) * 5;
    @endphp
    <div class="col-9 mt-3">
        <form class="form-search" action="{{route('menus.list')}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>

                <div class="col-3">
                    <button type="submit" class="btn btn-info btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
            <tr>
<!--                <th style="width: 50px">ID</th>-->
                <th>Name</th>
                <th>Parent</th>
                <th>Active</th>
                <th>Update</th>
                <th style="width: 200px">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($menus as $menu)
            <tr>
                <td>{{ $menu->name }}</td>
                <td>{{ $menu->categories->name ?? '' }}</td>
                <td>{!! \App\Helpers\Helper::active($menu->active) !!}</td>
                <td>{{ $menu->updated_at }}</td>
                <td>&nbsp;

                    <a class="btn btn-primary btn-sm" href="/admin/menus/edit/{{ $menu->id }}" >
                        <i class="fas fa-edit"></i>
                    </a>

                    <a class="btn btn-danger btn-sm" href="#" onclick="removeRow({{ $menu->id }}, '/admin/menus/destroy')">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $menus -> links() }}
@endsection

@section('footer')
    <script>
        CKEDITOR.replace( 'content' );
    </script>
@endsection
