@extends('admin.main')
@section('head')
    <script src="https://code.highcharts.com/highcharts.js"></script>
@endsection
@section('content')
    <h1></h1>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{\App\Models\Product::count()}}</h3>

                            <p>Sản Phẩm</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="/admin/products/list" class="small-box-footer">Xem<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{\App\Models\Menu::count()}}</h3>

                            <p>Danh Mục</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="/admin/menus/list" class="small-box-footer">Xem<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{\App\Models\Customer::count()}}</h3>

                            <p>Đơn Hàng</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="/admin/customers" class="small-box-footer">Xem<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{\App\Models\Slider::count()}}</h3>

                            <p>Slider</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="/admin/sliders/list" class="small-box-footer">Xem<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{\App\Models\Role::count()}}</h3>

                            <p>Role</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="/admin/roles/list" class="small-box-footer">Xem<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{\App\Models\User::count()}}</h3>

                            <p>Người Dùng</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="/admin/users/list" class="small-box-footer">Xem<i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

        <div class="container-fluid">
            <div class="row">
                <div class="card card-primary mt-3" style="width: 100%;">
                    <div class="card-header">
                        <h3 class="card-title">Doanh thu</h3>
                    </div>
                    <div class="card-body">
                        <div class="text-center">
                            Doanh thu tháng: <input style="margin-right: 5px" name="date" type="month" id="statistic"
                                                    value="{{ \Carbon\Carbon::now()->format('Y-m') }}"
                                                    data-action="{{ route('statistic.statisticSalesByMonth') }}">
                            là: <span id="total-statistic"></span>
                        </div>
                        <div id="container"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            let element = $('#statistic');
            let data = element.val();
            let url = element.data('action');
            drawCrLineChart(url, data);
            $(document).on('change', '#statistic', function () {
                let data = $(this).val();
                let url = $(this).data('action');
                drawCrLineChart(url, data);
            })
        });

        function drawCrLineChart(url, data) {
            $.get(url, {date: data}, (res) => {
                let {data} = res;
                let {total} = res;
                drawChartStatisticSale(data);
                $('#total-statistic').html('').append(total + ' VNĐ');
            });
        }

        /**
         * Show Chart
         */
        function drawChartStatisticSale(data) {
            Highcharts.chart('container', {
                title: {
                    text: ''
                },
                yAxis: {
                    title: {
                        text: 'VNĐ'
                    }
                },

                xAxis: {
                    accessibility: {
                        rangeDescription: 'Range: 1 to 30'
                    }
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 1
                    }
                },

                series: [{
                    name: 'Doanh thu',
                    data: Object.values(data),
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        }
    </script>
@endsection
