<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CartController;

Route::name('admin.carts.')->prefix('admin/carts/')->middleware(['auth', 'check.role:admin'])->group(function () {
    Route::post('update-status-order/{customer}', [CartController::class, 'updateStatusOrder'])
        ->name('update-status-cart');
});
