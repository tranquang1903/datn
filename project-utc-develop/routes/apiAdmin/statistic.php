<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\StatisticController;

Route::prefix('statistic')->name('statistic.')->group(function () {
    Route::get('/sale-by-month/', [StatisticController::class, 'statisticSalesByMonth'])->name('statisticSalesByMonth');
});
