<?php

use App\Http\Controllers\Admin\CartController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\Users\LoginController;
use App\Http\Controllers\User\ClientCartController;
use App\Http\Controllers\User\ClientContactController;
use App\Http\Controllers\User\ClientLoginController;
use App\Http\Controllers\User\ClientMainController;
use App\Http\Controllers\User\ClientMenuController;
use App\Http\Controllers\User\ClientProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/users/login', [LoginController::class, 'index'])->name('admin.login');
Route::post('admin/users/login/store', [LoginController::class, 'store']);

Route::middleware(['auth'])->group(function () {

    Route::prefix('admin')->group(function () {
        Route::get('/', [MainController::class, 'index'])->name('admin');
        Route::get('main', [MainController::class, 'index']);
        /*Route::get('/', [MainController::class, 'show']);*/
        Route::get('logout', [LoginController::class, 'logout']);

        #Menu
        Route::prefix('menus')->name('menus.')->group(function () {
            Route::get('add', [MenuController::class, 'create'])
                ->middleware('check.permission:create-category')
                ->name('create');
            Route::post('add', [MenuController::class, 'store'])
                ->middleware('check.permission:create-category')
                ->name('store');
            Route::get('list', [MenuController::class, 'index'])
                ->name('list');
            Route::get('edit/{menu}', [MenuController::class, 'show'])
                ->middleware('check.permission:update-category')
                ->name('show');
            Route::post('edit/{menu}', [MenuController::class, 'update'])
                ->middleware('check.permission:update-category')
                ->name('update');
            Route::delete('destroy', [MenuController::class, 'destroy'])
                ->middleware('check.permission:delete-category')
                ->name('destroy');

        });

        #Product
        Route::prefix('products')->name('products.')->group(function () {
            Route::get('add', [ProductController::class, 'create'])
                ->name('create');
            Route::post('add', [ProductController::class, 'store'])
                ->middleware('check.permission:create-product')
                ->name('store');
            Route::get('list', [ProductController::class, 'index'])
                ->name('list');
            Route::get('edit/{product}', [ProductController::class, 'show'])
                ->middleware('check.permission:show-product')
                ->name('show');
            Route::post('edit/{product}', [ProductController::class, 'update'])
                ->middleware('check.permission:update-product')
                ->name('update');
            Route::DELETE('destroy', [ProductController::class, 'destroy'])
                ->middleware('check.permission:delete-product')
                ->name('destroy');
        });

        #Slider
        Route::prefix('sliders')->name('sliders.')->group(function () {
            Route::get('add', [SliderController::class, 'create'])
                ->name('create');
            Route::post('add', [SliderController::class, 'store'])
                ->middleware('check.permission:create-slider')
                ->name('store');
            Route::get('list', [SliderController::class, 'index'])
                ->name('list');
            Route::get('edit/{slider}', [SliderController::class, 'show'])
                ->middleware('check.permission:show-slider')
                ->name('show');
            Route::post('edit/{slider}', [SliderController::class, 'update'])
                ->middleware('check.permission:update-slider')
                ->name('update');
            Route::DELETE('destroy', [SliderController::class, 'destroy'])
                ->middleware('check.permission:delete-slider')
                ->name('destroy');
        });

        #Role
        Route::prefix('roles')->name('roles.')->group(function () {
            Route::get('add', [RoleController::class, 'create'])
                ->name('create');
            Route::post('add', [RoleController::class, 'store'])
                ->middleware('check.permission:create-role')
                ->name('store');
            Route::get('list', [RoleController::class, 'index'])
                ->name('list');
            Route::get('edit/{role}', [RoleController::class, 'edit'])
                ->name('edit')
                ->middleware('check.permission:update-role');
            Route::put('/{role}', [RoleController::class, 'update'])
                ->name('update')
                ->middleware('check.permission:update-role');
            Route::DELETE('destroy', [RoleController::class, 'destroy'])
                ->middleware('check.permission:delete-role')
                ->name('destroy');
        });

        #User in Admin
        Route::prefix('users')->name('users.')->group(function () {
            Route::get('add', [UserController::class, 'create'])
                ->name('create');
            Route::post('add', [UserController::class, 'store'])
                ->middleware('check.permission:create-user')
                ->name('store');
            Route::get('list', [UserController::class, 'index'])
                ->name('list');
            Route::get('edit/{user}', [UserController::class, 'edit'])
                ->name('edit')
                ->middleware('check.permission:update-user');
            Route::put('/{user}', [UserController::class, 'update'])
                ->name('update')
                ->middleware('check.permission:update-user');
            Route::DELETE('destroy', [UserController::class, 'destroy'])
                ->middleware('check.permission:delete-user')
                ->name('destroy');
        });


        #Upload
        Route::post('upload/services', [UploadController::class, 'store']);

        #Cart
        Route::get('customers', [CartController::class, 'index']);
        Route::get('customers/view/{customer}', [CartController::class, 'show'])
            ->middleware('check.role:admin');
        include 'apiAdmin/statistic.php';
    });
});

Route::get('/', [ClientMainController::class, 'index'])->name('client');
Route::post('/services/load-products', [ClientMainController::class, 'loadProduct']);

/*lay ra danh sach san pham*/
Route::get('danh-muc/{id}-{slug}.html', [ClientMenuController::class, 'index']);

/*hien ra chi tiet san pham*/
Route::get('san-pham/{id}-{slug}.html', [ClientProductController::class, 'index']);

/*them gio hang*/
Route::post('/add-cart', [ClientCartController::class, 'index']);
Route::get('carts', [ClientCartController::class, 'show']);
Route::post('update-cart', [ClientCartController::class, 'update']);
Route::get('carts/delete/{id}', [ClientCartController::class, 'remove']);
Route::post('carts', [ClientCartController::class, 'addCart']);

/*login_register client*/
Route::get('user/login', [ClientLoginController::class, 'index']);
Route::post('user/login/store', [ClientLoginController::class, 'store']);
Route::get('user/logout', [ClientLoginController::class, 'logout']);
Route::get('user/register', [ClientLoginController::class, 'register']);
Route::post('user/register/create', [ClientLoginController::class, 'create']);

/*contact*/
Route::get('user/contact', [ClientContactController::class, 'index']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

include "apiAdmin/transaction.php";
