<?php

namespace App\Rules;

use App\Models\Product;
use Illuminate\Contracts\Validation\Rule;

class CheckAddToCart implements Rule
{
    protected $qty;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($qty)
    {
        $this->qty = $qty;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $product = Product::findOrFail($value);
        if($product->quantity == null){
            return false;
        }
        if($product->quantity == 0){
            return false;
        }
        if(((int)$product->quantity - (int)$this->qty) < 0){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sản phẩm đã hết hàng.';
    }
}
