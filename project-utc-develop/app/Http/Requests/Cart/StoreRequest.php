<?php

namespace App\Http\Requests\Cart;

use App\Models\Product;
use App\Rules\CheckAddToCart;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', new CheckAddToCart($this->input('num-product'))]
        ];
    }
}
