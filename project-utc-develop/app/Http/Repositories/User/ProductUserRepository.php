<?php

namespace App\Http\Repositories\User;

use App\Http\Repositories\BaseRepository;
use App\Models\Product;

class ProductUserRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    public function getProductNew()
    {
        return $this->model->active()->getProductNew()->take(4)->get();
    }

    public function productRelated($cates)
    {
        return $this->model->active()->productRelated($cates)->take(8)->get();
    }
}
