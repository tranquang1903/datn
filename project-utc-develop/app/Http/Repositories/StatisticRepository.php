<?php

namespace App\Http\Repositories;

use App\Models\Customer;
use Carbon\Carbon;

class StatisticRepository extends BaseRepository
{
    public function model()
    {
        return Customer::class;
    }

    public function statisticSalesByMonth($data)
    {
        $statistic = [];
        $total = 0;
        $daysInMonth = Carbon::create($data['year'], $data['month'])->daysInMonth;
        for ($i = 1; $i <= $daysInMonth; $i++) {
            $customers = $this->model->withSuccess()->withMonth($data['month'])->withYear($data['year'])->withDay($i)->get();
            $sale = 0;
            foreach ($customers as $customer) {
                $sale += $customer->total_customer;
            }
            $statistic[$i] = $sale;
            $total += $sale;
        }
        return [
            'statistic' => $statistic,
            'total' => number_format($total)
        ];
    }
}
