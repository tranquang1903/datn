<?php

namespace App\Http\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->WithName($dataSearch['name'])
            ->WithMinPrice($dataSearch['min_price'])->WithMaxPrice($dataSearch['max_price'])
            ->latest('id')->paginate(5);
    }
}
