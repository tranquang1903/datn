<?php

namespace App\Http\Repositories;

use App\Models\Slider;

class SliderRepository extends BaseRepository
{

    public function model()
    {
        return Slider::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->latest('id')->paginate(15);
    }
}
