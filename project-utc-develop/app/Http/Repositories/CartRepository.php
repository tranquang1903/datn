<?php

namespace App\Http\Repositories;

use App\Models\Customer;

class CartRepository extends BaseRepository
{

    public function model()
    {
        return Customer::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->WithEmail($dataSearch['email'])
            ->latest('id')->paginate(5);
    }
}
