<?php

namespace App\Http\Repositories;

use App\Models\Menu;

class MenuRepository extends BaseRepository
{

    public function model()
    {
        return Menu::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->latest('id')->paginate(10);
    }

    public function getAllCategoryRelated($categoryIds): array
    {
        $categories = [];
        foreach ($categoryIds as $categoryId) {
            foreach ($this->model->find($categoryId)->children()->pluck('id')->toArray() as $item) {
                $categories[] = $item;
            }
            foreach ($this->model->find($categoryId)->parentCategory()->pluck('id')->toArray() as $item) {
                $categories[] = $item;
            }
        }
        return $categories;
    }
}
