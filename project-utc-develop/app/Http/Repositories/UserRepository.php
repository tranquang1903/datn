<?php

namespace App\Http\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->WithEmail($dataSearch['email'])
            ->latest('id')->paginate(5);
    }
}
