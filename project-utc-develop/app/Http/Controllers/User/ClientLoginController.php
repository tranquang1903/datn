<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\LoginClientRequest;
use App\Http\Requests\Client\RegisterClientRequest;
use App\Http\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ClientLoginController extends Controller
{
    protected $userService;

    /**
     * @param $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function index()
    {
        return view('user.login.login', [
            'title' => 'Đăng Nhập'
        ]);
    }

    public function store(LoginClientRequest $request)
    {
        /*$this->validate($request, [
            'email' => 'required|email:filter'
        ]);*/
        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ], $request->input('remember'))) {

            return redirect()->route('client');
        }

        Session::flash('error', 'Email hoặc Password không đúng');
        return redirect()->back();
    }
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/user/login');
    }

    public function register()
    {
        return view('user.login.register', [
            'title' => 'Đăng Ký'
        ]);
    }

    public function create(RegisterClientRequest $request)
    {
        $result = $this->userService->create($request);
        if ($result) {
            return redirect('/user/login')
                ->with('success', 'Tạo Thành Công');

        }
        return redirect()->back();
    }
}
