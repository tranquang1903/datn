<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientContactController extends Controller
{
    public function index()
    {
        return view('user.contact.contact', [
            'title' => 'Thông Tin Shop'
        ]);
    }
}
