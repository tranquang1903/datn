<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Statistic\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StatisticController extends Controller
{
    protected $statisticService;

    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    public function statisticSalesByMonth(Request $request)
    {
        $data = $this->statisticService->statisticSalesByMonth($request);
        return response()->json([
            'data' => $data['statistic'],
            'total' => $data['total']
        ], Response::HTTP_OK);
    }
}
