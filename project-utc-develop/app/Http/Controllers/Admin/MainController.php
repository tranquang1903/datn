<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cart;

class MainController extends Controller
{
    public function index()
    {
        return view('admin.home', [
           'title' => 'Trang Quản trị Admin'
        ]);
    }

    /*public function show()
    {
        $getMonths[] = Cart::where('created_at','>=','2022-01-01')
            ->where('created_at','<=', '2022-01-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-02-01')
            ->where('created_at','<=', '2022-02-29')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-03-01')
            ->where('created_at','<=', '2022-03-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-04-01')
            ->where('created_at','<=', '2022-04-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-05-01')
            ->where('created_at','<=', '2022-05-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-06-01')
            ->where('created_at','<=', '2022-06-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-07-01')
            ->where('created_at','<=', '2022-07-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-08-01')
            ->where('created_at','<=', '2022-08-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-09-01')
            ->where('created_at','<=', '2022-09-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-10-01')
            ->where('created_at','<=', '2022-10-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-11-01')
            ->where('created_at','<=', '2022-11-31')
            ->sum('price');
        $getMonths[] = Cart::where('created_at','>=','2022-12-01')
            ->where('created_at','<=', '2022-12-31')
            ->sum('price');

        return response()->json(['getMonths' => $getMonths], 201);
    }*/
}
