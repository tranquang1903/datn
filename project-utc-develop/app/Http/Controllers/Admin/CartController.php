<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Cart\CartService;
use App\Jobs\UpdateQuantityProductJob;
use App\Models\Customer;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $cartService;

    /**
     * @param $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index(Request $request)
    {
        $customers = $this->cartService->search($request);
        return view('admin.cart.customer', [
            'title' => 'Danh Sách Đơn Đặt Hàng',
            'customers' => $customers
        ]);
    }

    public function show(Customer $customer)
    {
        $carts = $this->cartService->getProductForCart($customer);

        return view('admin.cart.detail', [
            'title' => 'Chi Tiết Đơn Hàng: ' . $customer->name,
            'customer' => $customer,
            'carts' => $carts
        ]);
    }

    public function updateStatusOrder(Request $request, Customer $customer)
    {
        $customer->update(['status' => $request->status]);
        if ($request->status == Customer::SUCCESS) {
            $this->dispatch(new UpdateQuantityProductJob($customer));
        }
        return response()->json(['success' => 'Cập nhật trạng thái thành công!!!']);
    }
}
