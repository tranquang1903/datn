<?php

namespace App\Http\Services\Product\User;

use App\Http\Repositories\MenuRepository;
use App\Http\Repositories\User\ProductUserRepository;
use App\Models\Product;

class ProductUserService
{
    protected $productUserRepository;
    protected $menuRepository;

    const LIMIT = 4;

    /**
     * @param ProductUserRepository $productUserRepository
     * @param MenuRepository $menuRepository
     */
    public function __construct(ProductUserRepository $productUserRepository, MenuRepository $menuRepository)
    {
        $this->productUserRepository = $productUserRepository;
        $this->menuRepository = $menuRepository;
    }

    public function get($page = null)
    {
        return Product::select('id', 'name', 'price', 'price_sale', 'thumb')
            ->where('active', 1)
            ->orderByDesc('id')
            ->when($page != null, function ($query) use ($page) {
                $query->offset($page * self::LIMIT);
            })
            ->limit(self::LIMIT)
            ->get();
    }


    public function show($id)
    {
        return Product::where('id', $id)
            ->where('active', 1)
            ->with('menus')
            ->firstOrFail();
    }

    public function more($id)
    {
        return Product::select('id', 'name', 'price', 'price_sale', 'thumb')
            ->where('active', 1)
            ->where('id', '!=', $id)
            ->orderByDesc('id')
            ->limit(4)
            ->get();
    }

    public function getProductNew()
    {
        return $this->productUserRepository->getProductNew();
    }

    public function productFeatured($product)
    {
        $categoryIds = $product->menus()->get()->pluck('id')->toArray();
        $cate = $this->menuRepository->getAllCategoryRelated($categoryIds);
        return $this->productUserRepository->productRelated($cate);
    }
}
