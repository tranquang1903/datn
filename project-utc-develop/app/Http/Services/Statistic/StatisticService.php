<?php

namespace App\Http\Services\Statistic;

use App\Http\Repositories\StatisticRepository;
use Carbon\Carbon;

class StatisticService
{
    protected $statisticRepository;

    public function __construct(StatisticRepository $statisticRepository)
    {
        $this->statisticRepository = $statisticRepository;
    }

    public function statisticSalesByMonth($request)
    {
        $date = Carbon::create($request->date);
        $data['month'] = $date->format('m');
        $data['year'] = $date->format('Y');
        return $this->statisticRepository->statisticSalesByMonth($data);
    }
}
