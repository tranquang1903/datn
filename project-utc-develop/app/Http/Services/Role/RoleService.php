<?php

namespace App\Http\Services\Role;

use App\Http\Repositories\RoleRepository;
use App\Models\Role;

class RoleService
{
    protected $roleRepository;

    /**
     * @param $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->roleRepository->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission'] = $request->permission ?? [];
        $role = $this->roleRepository->create($dataCreate);
        $role->attachPermission($dataCreate['permission']);
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataCreate['permission'] = $request->permission ?? [];
        $role->update($dataUpdate);
        $role->syncPermission($dataCreate['permission']);
        return $role;
    }

    public function destroy($request)
    {
        $role = Role::where('id' , $request->input('id'))->first();
        if ($role) {
            $role->detachUser();
            $role->detachPermission();
            $role->delete();
            return true;
        }
        return false;
    }
}
