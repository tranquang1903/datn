<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'display_name'
    ];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function hasPermission($permissionName)
    {
        return $this->permissions->contains('name', $permissionName);
    }

    public function attachPermission($permissionID)
    {
        return $this->permissions()->attach($permissionID);
    }

    public function syncPermission($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }

    public function detachUser()
    {
        return $this->users()->detach();
    }

    public function detachPermission()
    {
        return $this->permissions()->detach();
    }
}
