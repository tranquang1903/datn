<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'address',
        'email',
        'content',
        'status'
    ];

    protected $appends = [
        'total_customer'
    ];

    const WAIT = '0';
    const SUCCESS = '1';
    const FAIL = '2';
    const DOING = '3';
    const TEXT_WAIT = 'Chờ xác nhận';
    const TEXT_SUCCESS = 'Giao hàng thành công';
    const TEXT_FAIL = 'Giao hàng thất bại';
    const TEXT_DOING = 'Đang giao';

    public function carts()
    {
        return $this->hasMany(Cart::class, 'customer_id', 'id');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', 'like', '%' . $email . '%') : null;
    }

    public function getStatusOrderAttribute()
    {
        if ($this->status == self::WAIT) {
            return self::TEXT_WAIT;
        }
        if ($this->status == self::SUCCESS) {
            return self::TEXT_SUCCESS;
        }
        if ($this->status == self::FAIL) {
            return self::TEXT_FAIL;
        }
        if ($this->status == self::DOING) {
            return self::TEXT_DOING;
        }
    }

    public function scopeWithMonth($query, $month)
    {
        return $month ? $query->whereMonth('created_at', $month) : null;
    }

    public function scopeWithYear($query, $year)
    {
        return $year ? $query->whereYear('created_at', $year) : null;
    }

    public function scopeWithDay($query, $day)
    {
        return $day ? $query->whereDay('created_at', $day) : null;
    }

    public function scopeWithSuccess($query)
    {
        return $query->where('status', self::SUCCESS);
    }

    public function getTotalCustomerAttribute()
    {
        $total = 0;
        if ($this->carts->isNotEmpty()) {
            foreach ($this->carts as $cart) {
                $total += (int)$cart->price * (int)$cart->pty;
            }
        }
        return $total;
    }
}
