<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::updateOrCreate(['name' => 'admin'], ['display_name' => 'Người quản trị']);
        $roleUser = Role::updateOrCreate(['name' => 'user'], ['display_name' => 'Khách hàng']);
        $roleManageUser = Role::updateOrCreate(['name' => 'manage-user'], ['display_name' => 'QUản lý người dùng']);


        $permissionCreateUser = Permission:: updateOrCreate(['name' => 'create-user'], ['display_name' => 'Tạo người dùng', 'group_name' => 'User']);
        $permissionUpdateUser = Permission:: updateOrCreate(['name' => 'update-user'], ['display_name' => 'Cập nhật người dùng', 'group_name' => 'User']);
        $permissionShowUser = Permission:: updateOrCreate(['name' => 'show-user'], ['display_name' => 'Xem người dùng', 'group_name' => 'User']);
        $permissionDeleteUser = Permission:: updateOrCreate(['name' => 'delete-user'], ['display_name' => 'Xóa người dùng', 'group_name' => 'User']);
        $permissionCreateProduct = Permission:: updateOrCreate(['name' => 'create-product'], ['display_name' => 'Tạo san pham', 'group_name' => 'product']);
        $permissionUpdateProduct = Permission:: updateOrCreate(['name' => 'update-product'], ['display_name' => 'Cập nhật san pham', 'group_name' => 'product']);
        $permissionShowProduct = Permission:: updateOrCreate(['name' => 'show-product'], ['display_name' => 'Xem san pham', 'group_name' => 'product']);
        $permissionDeleteProduct = Permission:: updateOrCreate(['name' => 'delete-product'], ['display_name' => 'Xóa san pham', 'group_name' => 'product']);
        $permissionCreateCategory = Permission:: updateOrCreate(['name' => 'create-category'], ['display_name' => 'Tạo danh muc', 'group_name' => 'Category']);
        $permissionUpdateCategory = Permission:: updateOrCreate(['name' => 'update-category'], ['display_name' => 'Cập nhật danh muc', 'group_name' => 'Category']);
        $permissionShowCategory = Permission:: updateOrCreate(['name' => 'show-category'], ['display_name' => 'Xem danh muc', 'group_name' => 'Category']);
        $permissionDeleteCategory = Permission:: updateOrCreate(['name' => 'delete-category'], ['display_name' => 'Xóa danh muc', 'group_name' => 'Category']);
        $permissionCreateRole = Permission:: updateOrCreate(['name' => 'create-role'], ['display_name' => 'Tạo quyền', 'group_name' => 'Role']);
        $permissionUpdateRole = Permission:: updateOrCreate(['name' => 'update-role'], ['display_name' => 'Cập quyền', 'group_name' => 'Role']);
        $permissionShowRole = Permission:: updateOrCreate(['name' => 'show-role'], ['display_name' => 'Xem quyền', 'group_name' => 'Role']);
        $permissionDeleteRole = Permission:: updateOrCreate(['name' => 'delete-role'], ['display_name' => 'Xóa quyền', 'group_name' => 'Role']);
        $permissionCreateSlider = Permission:: updateOrCreate(['name' => 'create-slider'], ['display_name' => 'Tạo slider', 'group_name' => 'Slider']);
        $permissionUpdateSlider = Permission:: updateOrCreate(['name' => 'update-slider'], ['display_name' => 'Cập slider', 'group_name' => 'Slider']);
        $permissionShowSlider = Permission:: updateOrCreate(['name' => 'show-slider'], ['display_name' => 'Xem slider', 'group_name' => 'Slider']);
        $permissionDeleteSlider = Permission:: updateOrCreate(['name' => 'delete-slider'], ['display_name' => 'Xóa slider', 'group_name' => 'Slider']);

        $roleManageUser->permissions()->attach([$permissionCreateUser->id, $permissionUpdateUser->id, $permissionShowUser->id, $permissionDeleteUser->id]);


        $admin = User::whereEmail('admin@gmail.com')->first();

        if (!$admin) {
            $admin = User::factory()->create(['email' => 'admin@gmail.com', 'password' => Hash::make('12345678')]);
        }

        $admin->roles()->sync($roleAdmin->id);
    }
}
